import {applyMiddleware} from 'redux';
import UserMiddleware from './UserMiddleware';
import GlossaryMiddleware from './GlossaryMiddleware';
import PromiseMiddleware from "./PromiseMiddleware";
import EditorMiddleware from "./EditorMiddleware";

const middleware = applyMiddleware(UserMiddleware, GlossaryMiddleware, EditorMiddleware, PromiseMiddleware);

export default middleware;