import {isFetching} from "../helpers/Utilities";

const UserMiddleware = store => next => action => {
	// Cancel request if app is fetching
	if (isFetching(store) === true) return next(action);

	switch (action.type) {
		default:
	}

	next(action);
};

export default UserMiddleware;