import {
	GLOSSARY_FETCH_LIST,
	GLOSSARY_NEW_ITEM,
	GLOSSARY_DELETE_ITEM
} from './../constants/ActionTypes';

import RestApi from "../helpers/RestApi";
import {setFetching} from "../actions/AppActions";
import {isFetching} from "../helpers/Utilities";

const GlossaryMiddleware = store => next => action => {
	// Cancel request if app is fetching
	if (isFetching(store) === true) return next(action);

	let data = action.payload;

	switch (action.type) {
		case GLOSSARY_FETCH_LIST:
			store.dispatch(setFetching(true));

			data.request = RestApi.glossary.items();
			break;

		case GLOSSARY_NEW_ITEM:
			store.dispatch(setFetching(true));

			data.request = RestApi.glossary.create(data.title, data.content);
			break;

		case GLOSSARY_DELETE_ITEM:
			store.dispatch(setFetching(true));

			data.request = RestApi.glossary.delete(data.id);
			break;
		default:
	}

	next(action);
};

export default GlossaryMiddleware;