import {
	PENDING,
	DONE,
	ERROR
} from './../constants/StatusTypes';

import {isPromise} from "../helpers/Utilities";
import StatusMessage from "../helpers/StatusMessage";
import {setFetching} from "../actions/AppActions";

const PromiseMiddleware = store => next => action => {

	const data = action.payload;

	// Check payload if that's a promise
	if (isPromise(data.request)) {
		StatusMessage(store, action, PENDING);
		data.request
			.then(response => {
				data.request = response;
				StatusMessage(store, action, DONE);
				store.dispatch(setFetching(false));
				next(action);
			})
			.catch(error => {
				data.error = error;
				StatusMessage(store, action, ERROR);
				store.dispatch(setFetching(false));
				next(action);
			});
	} else {
		next(action);
	}
};

export default PromiseMiddleware;