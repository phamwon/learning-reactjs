import {
	EDITOR_EDIT_POST
} from './../constants/ActionTypes';
import RestApi from "../helpers/RestApi";
import {setFetching} from "../actions/AppActions";
import {isFetching} from "../helpers/Utilities";

const EditorMiddleware = store => next => action => {
	// Cancel request if app is fetching
	if (isFetching(store) === true) return next(action);

	let data = action.payload;

	switch (action.type) {
		case EDITOR_EDIT_POST:
			store.dispatch(setFetching(true));

			data.request = RestApi.glossary.edit(data.currentId, data.content);
			break;

		default:
	}

	next(action);
};

export default EditorMiddleware;