import {
	USER_LOGIN,
	USER_LOGOUT
} from './../constants/ActionTypes';

export const login = (username, password) => ({
	type: USER_LOGIN,
	payload: {
		username,
		password
	}
});

export const logout = () => ({
	type: USER_LOGOUT,
	payload: {}
});