import {
	APP_UPDATE_MESSAGE,
	APP_SET_FETCHING_STATUS
} from './../constants/ActionTypes';

export const updateMessage = message => ({
	type: APP_UPDATE_MESSAGE,
	payload: {message}
});

export const setFetching = status => ({
	type: APP_SET_FETCHING_STATUS,
	payload: {
		isFetching: status
	}
});