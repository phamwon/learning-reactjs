import {
	GLOSSARY_TOGGLE_UNDERSTOOD,
	GLOSSARY_FETCH_LIST,
	GLOSSARY_REMOVE_LIST,
	GLOSSARY_DELETE_ITEM,
	GLOSSARY_NEW_ITEM
} from './../constants/ActionTypes';

export const toggleUnderstood = id => ({
	type: GLOSSARY_TOGGLE_UNDERSTOOD,
	payload: {id}
});

export const fetchList = () => {
	return {
		type: GLOSSARY_FETCH_LIST,
		payload: {
			id: ''
		}
	}
};

export const removeList = () => {
	return {
		type: GLOSSARY_REMOVE_LIST,
		payload: {
			items: []
		}
	}
};

export const createItem = (title, content) => {
	return {
		type: GLOSSARY_NEW_ITEM,
		payload: {title, content}
	}
};

export const deleteItem = id => {
	return {
		type: GLOSSARY_DELETE_ITEM,
		payload: {id}
	}
};