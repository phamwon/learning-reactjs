import {
	EDITOR_EDIT_POST,
	EDITOR_SHOW_IT,
	EDITOR_HIDE_IT
} from './../constants/ActionTypes';

export const showEditor = currentId => {
	return {
		type: EDITOR_SHOW_IT,
		payload: {currentId}
	}
};

export const hideEditor = currentId => {
	return {
		type: EDITOR_HIDE_IT,
		payload: {currentId}
	}
};

export const editPost = (currentId, content) => {
	return {
		type: EDITOR_EDIT_POST,
		payload: {currentId, content}
	}
};