import axios from 'axios';

axios.defaults.headers.common = {};
axios.defaults.headers.common.accept = 'application/x-www-form-urlencoded';

// const userToken = sessionStorage.getItem('userToken');

// const headers = {
// 	// Authorization: `Basic ${userToken}`,
// };

const defaultConfigs = {
	baseURL: 'http://localhost/api',
	responseType: 'json',
	// headers: headers
};

let realtimeConfigs = {
	...defaultConfigs,
};

const Requester = {
	read: (apiName, configs = {}) => axios.get(apiName, {...realtimeConfigs, ...configs}),
	create: (apiName, data, configs = {}) => axios.post(apiName, data, {...realtimeConfigs, ...configs}),
	update: (apiName, data, configs = {}) => axios.put(apiName, data, {...realtimeConfigs, ...configs}),
	delete: (apiName, configs = {}) => axios.delete(apiName, {...realtimeConfigs, ...configs}),
};

export {Requester, realtimeConfigs};