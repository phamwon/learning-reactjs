import {
	API_USER,
	API_POSTS
} from "../constants/ApiName";

import {Requester} from "./Requester";

const RestApi = {
	users: {
		login: (username, password) => Requester.create(API_USER, {username, password}),
		logout: () => Requester.delete(API_USER),
	},
	glossary: {
		items: () => Requester.read(API_POSTS),
		create: (title, content) => Requester.create(API_POSTS, {title, content}),
		edit: (id, content) => Requester.update(API_POSTS,{id, content}),
		delete: id => Requester.delete(API_POSTS,{id}),
	}
};

export default RestApi;