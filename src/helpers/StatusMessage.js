import {
	USER_LOGIN,
	USER_LOGOUT,
	GLOSSARY_FETCH_LIST,
	GLOSSARY_DELETE_ITEM,
	EDITOR_EDIT_POST,
	GLOSSARY_NEW_ITEM
} from './../constants/ActionTypes';

import {
	PENDING,
	DONE,
	ERROR
} from './../constants/StatusTypes';

import {updateMessage} from "../actions/AppActions";

const sendMessage = (store, message) => store.dispatch(updateMessage(message));

const StatusMessage = (store, action, status) => {
	let message;
	let data = action.payload;

	// Default message
	switch (status) {
		case DONE:
			message = data.request.message;
			break;
		case ERROR:
			message = data.error.message || '';
			break;
		default:
	}

	// Message by action type
	switch (action.type) {
		case USER_LOGIN:
			message = status === PENDING ? 'Logging...' : message;
			break;
		case USER_LOGOUT:
			message = status === PENDING ? 'Logout...' : message;
			break;
		case EDITOR_EDIT_POST:
			message = status === PENDING ? 'Updating post...' : message;
			break;
		case GLOSSARY_FETCH_LIST:
			message = status === PENDING ? 'Fetching post...' : message;
			break;
		case GLOSSARY_NEW_ITEM:
			message = status === PENDING ? 'Posting...' : message;
			break;
		case GLOSSARY_DELETE_ITEM:
			message = status === PENDING ? 'Deleting post...' : message;
			break;
		default:
			message = status === PENDING ? `Unknown status !? Status: ${status}`: message;
	}

	// console.log(status, action.type, message)
	sendMessage(store, message);
};

export default StatusMessage;
