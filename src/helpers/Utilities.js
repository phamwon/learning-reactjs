// Convert object to array
const objectToArray = object => Array.from(Object.keys(object), i => object[i]);

// Check is promise
const isPromise = object => object && typeof object.then === 'function';

// HTML Decode
const htmlDecode = html => {
	let elm = document.createElement('textarea');
	elm.innerHTML = html;
	return elm.value;
};

// Check app is fetching
const isFetching = store => (store.getState().AppReducer.isFetching);

export {objectToArray, isPromise, htmlDecode, isFetching};