import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';

// CSS
import './App.scss';

// Redux Store
import store from './stores';

// Components
import MainContent from './components/MainContent';

// Stateless Components
const NotFound = () => (<div>Page Not Found</div>);

//	Stateful Components
class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<BrowserRouter>
					<Switch>
						<Route path='/' exact component={MainContent}/>
						<Route component={NotFound}/>
					</Switch>
				</BrowserRouter>
			</Provider>
		);
	}
}

export default App;
