import {
	EDITOR_EDIT_POST,
	EDITOR_SHOW_IT,
	EDITOR_HIDE_IT
} from '../constants/ActionTypes';

const initialState = {
	currentId: null,
	content: null,
	isEditing: false,
	request: null,
	error: null,
};

export default function (state = initialState, action) {
	let data = action.payload;

	switch (action.type) {
		case EDITOR_EDIT_POST:
			console.log(state, data);
			break;

		case EDITOR_SHOW_IT:
			data.isEditing = true;
			break;

		case EDITOR_HIDE_IT:
			data.isEditing = false;
			break;

		default:
			return state;
	}

	return {...state, ...data};
}