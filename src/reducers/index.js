import {combineReducers} from 'redux';
import GlossaryReducer from './GlossaryReducer';
import UserReducer from './UserReducer';
import AppReducer from './AppReducer';
import EditorReducer from './EditorReducer';

export default combineReducers({AppReducer, UserReducer, GlossaryReducer, EditorReducer});