import {
	USER_LOGIN,
	USER_LOGOUT,
	USER_INFO
} from './../constants/ActionTypes';

import {realtimeConfigs} from  './../helpers/Requester';

const isLogged = sessionStorage.getItem('isLogged') === 'true';
const userToken = sessionStorage.getItem('userToken');

const initialState = {
	token: userToken,
	userId: null,
	isLogged: isLogged,
	request: {}
};

export default function (state = initialState, action) {

	const data = action.payload;

	switch (action.type) {
		case USER_LOGIN:
			data.token = window.btoa(`${data.username}:${data.password}`);
			sessionStorage.setItem('userToken',  data.token);

			data.isLogged = data.token !== '';
			sessionStorage.setItem('isLogged', data.isLogged);

			// Update request header
			realtimeConfigs.headers.Authorization = `Basic ${data.token}`;

			return {...state, ...data};

		case USER_LOGOUT:
			data.token = '';
			data.userId = 0;
			data.isLogged = false;
			sessionStorage.clear();

			return {...state, ...data};

		case USER_INFO:
			return {...state};

		default:
			return state;
	}
}