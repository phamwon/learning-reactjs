import {
	APP_SET_FETCHING_STATUS,
	APP_UPDATE_MESSAGE
} from '../constants/ActionTypes';

const initialState = {
	message: '',
	isFetching: false
};

export default function (state = initialState, action) {
	const payload = action.payload;

	switch (action.type) {
		case APP_UPDATE_MESSAGE:
			return {...state, ...payload};

		case APP_SET_FETCHING_STATUS:
			return {...state, ...payload};

		default:
			return state;
	}
}