import {
	GLOSSARY_TOGGLE_UNDERSTOOD,
	GLOSSARY_FETCH_LIST,
	GLOSSARY_REMOVE_LIST
} from '../constants/ActionTypes';
import {objectToArray, htmlDecode} from "../helpers/Utilities";

const initialState = {
	items: [],
	request: {}
};

export default function (state = initialState, action) {
	const items = state.items;
	let data = action.payload;

	const itemId = items.findIndex(item => item.id === data.id);

	switch (action.type) {
		case GLOSSARY_TOGGLE_UNDERSTOOD:
			data.items = {...items};

			let isUnderstood = items[itemId].isUnderstood;

			data.items[itemId].isUnderstood = !isUnderstood;
			break;

		case GLOSSARY_FETCH_LIST:
			data.items = data.request.data.map((item, id) => {
				let title = item.title;
				let content = htmlDecode(item.content);
				let isUnderstood = false;

				return {id, title, content, isUnderstood};
			});
			break;

		case GLOSSARY_REMOVE_LIST:
			return {...data};

		default:
			return state;
	}

	data.items = objectToArray(data.items);

	return {...state, ...data};
}