// App Actions
export const APP_UPDATE_MESSAGE = 'APP_UPDATE_MESSAGE';
export const APP_SET_FETCHING_STATUS = 'APP_SET_FETCHING_STATUS';

// User Actions
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_INFO = 'USER_INFO';

// Glossary Actions
export const GLOSSARY_TOGGLE_UNDERSTOOD = 'GLOSSARY_TOGGLE_UNDERSTOOD';
export const GLOSSARY_FETCH_LIST = 'GLOSSARY_FETCH_LIST';
export const GLOSSARY_REMOVE_LIST = 'GLOSSARY_REMOVE_LIST';
export const GLOSSARY_DELETE_ITEM = 'GLOSSARY_DELETE_ITEM';
export const GLOSSARY_NEW_ITEM = 'GLOSSARY_NEW_ITEM';

// Editor Actions
export const EDITOR_EDIT_POST = 'EDITOR_EDIT_POST';
export const EDITOR_SHOW_IT = 'EDITOR_SHOW_IT';
export const EDITOR_HIDE_IT = 'EDITOR_HIDE_IT';