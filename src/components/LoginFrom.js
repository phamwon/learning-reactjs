import React, {Component} from 'react';
import {connect} from 'react-redux';
import {login} from '../actions/UserActions';

class LoginFrom extends Component {
	/*componentWillMount() {
		console.log('LoginFrom component WILL MOUNT!')
	}

	componentDidMount() {
		console.log('LoginFrom component DID MOUNT!')
	}

	componentWillReceiveProps(nextProps) {
		console.log('LoginFrom component WILL RECIEVE PROPS!', nextProps)
	}

	shouldComponentUpdate(nextProps, nextState) {
		console.log('LoginFrom component SHOULD UPDATE!', nextProps, nextState);
		return true;
	}

	componentWillUpdate(nextProps, nextState) {
		console.log('LoginFrom component WILL UPDATE!', nextProps, nextState);
	}

	componentDidUpdate(prevProps, prevState) {
		console.log('LoginFrom component DID UPDATE!', prevProps, prevState)
	}

	componentWillUnmount() {
		console.log('LoginFrom component WILL UNMOUNT!')
	}*/

	handleSubmit = event => {
		event.preventDefault();
		const {username, password} = event.target;
		const {login} = this.props;

		login(username.value, password.value);
	};

	render() {
		return (
			<div className='login-from'>
				<form onSubmit={this.handleSubmit}>
					<input name='username' placeholder='Username' type='text' value='phamwon' readOnly/>
					<input name='password' placeholder='Password' type='password' value='gDCx arfQ 7S1W 3BLL 48t2 I3gS' readOnly/>
					<input className='login-btn' type='submit' value='Login'/>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => state.UserReducer;

export default connect(mapStateToProps, {login})(LoginFrom);