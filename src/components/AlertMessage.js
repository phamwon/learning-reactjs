import React, {Component} from 'react';
import {connect} from 'react-redux';
import {updateMessage} from '../actions/AppActions';

class AlertMessage extends Component {
	render() {
		const {message} = this.props;

		return (
			<div className='alert-text'>
				<p dangerouslySetInnerHTML={{__html: message}}/>
			</div>
		);
	}
}

const mapStateToProps = state => state.AppReducer;

export default connect(mapStateToProps, {updateMessage})(AlertMessage);