import React, {Component} from 'react';
import {connect} from 'react-redux';

import {htmlDecode} from "../helpers/Utilities";

// Actions
import {updateMessage} from "../actions/AppActions";
import {showEditor, hideEditor, editPost} from '../actions/EditorActions';

class Editor extends Component {
	componentDidMount() {
		// Add event when mousedown document dom
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.currentId === this.props.id;
	}

	componentDidUpdate() {
		const {editor} = this.refs;
		const {isEditing, content} = this.props;
		if(!isEditing) editor.innerHTML = htmlDecode(content);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	handleClickOutside = event => {
		const {editor} = this.refs;
		const {currentId, id, content, isEditing, hideEditor, editPost} = this.props;

		if(editor.contains(event.target)) return;

		// Turn off editor mode when click outside the content
		isEditing && id !== currentId && hideEditor(id);

		// Update content if it's changed
		const editorContent = htmlDecode(editor.innerHTML);
		content !== editorContent && editPost(id, editorContent);
	};

	handleShowEditor = id => {
		return () => {
			const {isEditing, showEditor} = this.props;
			!isEditing && showEditor(id);
		}
	};

	render() {
		const {id, content} = this.props;

		return (
			<div className='editor'>
				<div ref='editor' contentEditable={true} dangerouslySetInnerHTML={{__html: content}}
				      onClick={this.handleShowEditor(id)}
				/>
			</div>
		);
	}
}

// Use makeMapStateToProps to pass the props
const makeMapStateToProps = (initialState, initialProps) => {
	const {id, content} = initialProps;

	return state => ({...state.EditorReducer, id, content});
};

export default connect(makeMapStateToProps, {updateMessage, showEditor, hideEditor, editPost})(Editor);