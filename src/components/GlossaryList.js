import React, {Component} from 'react';
import {connect} from 'react-redux';

import {updateMessage} from "../actions/AppActions";

// Components
import GlossaryItem from './GlossaryItem';

class GlossaryList extends Component {
	componentDidMount() {
		//console.log('GlossaryList component DID MOUNT!', this.props)

		const {items, updateMessage} = this.props;

		items.length < 1 && updateMessage('Please click fetch data button.')
	}

	shouldComponentUpdate(nextProps, nextState) {
		//console.log('GlossaryList component SHOULD UPDATE!', nextProps, nextState);

		const {items, updateMessage} = nextProps;

		items.length < 1 && updateMessage('Data is empty.')

		return items.length > 0;
	}

	render() {
		const {items} = this.props;

		return (
			<div className='glossary-list'>
				{items && items.length > 0 && items.map(item => (
					<GlossaryItem key={item.id || 0} id={item.id}/>
				))}
				{/*Glossary list is empty*/}
				{items.length < 1 && <p>Nothing here :(</p>}
			</div>
		);
	}
}

const mapStateToProps = state => state.GlossaryReducer;

export default connect(mapStateToProps, {updateMessage})(GlossaryList);