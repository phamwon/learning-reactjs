import React, {Component} from 'react';
import {connect} from 'react-redux';

import {createItem} from './../actions/GlossaryActions';

class CreatePostFrom extends Component {
	handleSubmit = event => {
		event.preventDefault();
		const {title, content} = event.target;
		const {createItem} = this.props;
		createItem(title.value, content.value);
	};

	render() {
		return (
			<div className='post-from'>
				<form onSubmit={this.handleSubmit}>
					<input name='title' placeholder='Post title' type='text'/>
					<input name='content' placeholder='Post content'/>
					<input className='create-btn' type='submit' value='Create post'/>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => state.UserReducer;

export default connect(mapStateToProps, {createItem})(CreatePostFrom);