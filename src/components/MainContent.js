import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchList, removeList} from '../actions/GlossaryActions';
import {logout} from '../actions/UserActions';
import {updateMessage} from "../actions/AppActions";

// Components
import LoginFrom from './LoginFrom';
import GlossaryList from './GlossaryList';
import AlertMessage from "./AlertMessage";
import CreatePostFrom from "./CreatePostFrom";

class MainContent extends Component {
	/*componentWillMount() {
		console.log('MainContent component WILL MOUNT!')
	}

	componentWillReceiveProps(nextProps) {
		console.log('MainContent component WILL RECIEVE PROPS!', nextProps)
	}

	componentDidUpdate(prevProps, prevState) {
		console.log('MainContent component DID UPDATE!', prevProps, prevState)
	}

	componentWillUnmount() {
		console.log('MainContent component WILL UNMOUNT!')
	}

	componentWillUpdate(nextProps, nextState) {
		console.log('MainContent component WILL UPDATE!', nextProps, nextState);
	}*/

	componentDidMount() {
		//console.log('MainContent component DID MOUNT!');

		const {updateMessage} = this.props;
		const {isLogged} = this.props.UserReducer;

		!isLogged && updateMessage('Please login to gain access to the glossary dictionary!');
	}

	shouldComponentUpdate(nextProps, nextState) {
		//console.log('MainContent component SHOULD UPDATE!', nextProps, nextState);

		const {updateMessage} = this.props;
		const {isLogged} = this.props.UserReducer;

		let isContinue = nextProps.UserReducer.isLogged !== isLogged;

		isContinue && updateMessage('Please login to gain access to the glossary dictionary!');

		return isContinue;
	}

	handleFetchList = () => {
		const {fetchList} = this.props;
		fetchList();
	};

	handleLogout = () => {
		const {removeList, logout} = this.props;
		removeList();
		logout();
	};

	render() {
		// console.log(this.props);
		const {isLogged} = this.props.UserReducer;
		return (
			<div className='app-main'>
				<div className='app-title'>
					<h1>Welcome to glossary dictionary</h1>
					<input type='checkbox' className='switch-color'/>
					{isLogged && <button className='fetch-btn' onClick={this.handleFetchList}>Fetch data</button>}
					{isLogged && <button className='logout-btn' onClick={this.handleLogout}>Logout</button>}
					{!isLogged && <LoginFrom/>}
				</div>
				<AlertMessage/>
				{isLogged && <GlossaryList/>}
				{isLogged && <CreatePostFrom/>}
			</div>
		);
	}
}

const mapStateToProps = state => state;

export default connect(mapStateToProps, {updateMessage, fetchList, removeList, logout})(MainContent);