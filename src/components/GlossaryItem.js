import React, {Component} from 'react';
import {connect} from 'react-redux';

// Actions
import {toggleUnderstood, deleteItem} from '../actions/GlossaryActions';

// Components
import Editor from './Editor';

class GlossaryItem extends Component {
	handleToggleUnderstood = id => {
		return () => {
			const {toggleUnderstood} = this.props;
			toggleUnderstood(id);
		}
	};

	handleDeletePost = id => {
		return () => {
			const {deleteItem} = this.props;
			deleteItem(id);
		}
	};

	render() {
		const {id, items} = this.props;
		const itemId = items.findIndex(item => item.id === id);
		const item = items[itemId];

		if(!item) {
			return ('');
		}

		return (
			<div>
				<div className={`glossary-item ${item.isUnderstood ? 'understood' : ''}`}>
					<p onClick={this.handleToggleUnderstood(item.id)}>{item.title}</p>
					<div className='glossary-btn'>
						{item.isUnderstood && <span className='delete-post-btn' role='img' aria-label='x'
						      onClick={this.handleDeletePost(item.id)}
						>❌️</span>}
					</div>
				</div>
				{item.content && <Editor id={item.id} content={item.content}/>}
			</div>
		);
	}
}

// Use makeMapStateToProps to pass the props
const makeMapStateToProps = (initialState, initialProps) => {
	const {id} = initialProps;

	return state => ({...state.GlossaryReducer, id});
};

export default connect(makeMapStateToProps, {toggleUnderstood, deleteItem})(GlossaryItem);